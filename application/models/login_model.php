<?php

class login_model extends CI_Model {

    var $title = '';
    var $content = '';
    var $date = '';

    function __construct() {
        // Model クラスのコンストラクタを呼び出す
        parent::__construct();
    }

    function check_user() {
        $sql = 'SELECT * FROM `user_info`';
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            // 結果データがありません。
            return NULL;
        }
    }

}
