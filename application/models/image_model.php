<?php

class Image_model extends CI_Model {


    function __construct() {
        // Model クラスのコンストラクタを呼び出す
        parent::__construct();
    }

    function get_user_diary() {
        $sql = 'SELECT * FROM `picture_diary` WHERE `user_id` = ? ORDER BY `create_at` DESC';
        $query = $this->db->query($sql,1);
        if ($query->num_rows() > 0) {
            return $query->result('array');
        } else {
            // 結果データがありません。
            return NULL;
        }
    }
    
    function get_friend_diary() {
        $sql = 'SELECT * FROM `picture_diary` WHERE `user_id` = ? ORDER BY `create_at` DESC';
        $query = $this->db->query($sql,2);
        if ($query->num_rows() > 0) {
            return $query->result('array');
        } else {
            // 結果データがありません。
            return NULL;
        }
    }
    
    function get_other_diary() {
        $sql = 'SELECT * FROM `picture_diary` WHERE `user_id` = ? ORDER BY `create_at` DESC';
        $query = $this->db->query($sql,3);
        if ($query->num_rows() > 0) {
            return $query->result('array');
        } else {
            // 結果データがありません。
            return NULL;
        }
    }
    
    function get_one_diary($picture_id) {
        $sql = 'SELECT * FROM `picture_diary` WHERE `picture_id` = ?';
        $query = $this->db->query($sql,array($picture_id));
        if ($query->num_rows() > 0) {
            return $query->result('array',0);
        } else {
            // 結果データがありません。
            return NULL;
        }
    }
    
    function insert_picture_diary($name){
        $date = date('Y-m-d H:i:s');
        $sql = 'INSERT INTO `picture_diary`(`user_id`, `name`, `open`, `create_at`) VALUES (?,?,?,?)';
        if ($this->db->query($sql,array('1',$name,'1',$date))) {
            return TRUE;
        } else {
            // 保存失敗。
            return FALSE;
        }
    }

}
