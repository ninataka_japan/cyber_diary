<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();

        $this->load->database();
        if ($this->db->conn_id === FALSE) {
            // データベースに接続されていません。
           
            //var_dump($res);
        } else {
            // データベースに接続されています。
           $this->load->model('image_model'); 
        }
    }
    
    //login画面
    public function admin(){
        $data['title'] = 'ログイン画面';
        $data['login'] = false;
        $this->load->view('header', $data);
        $this->load->view('login_view',$data);
        $this->load->view('footer',$data);
    }
    //userのアカウントが存在するかチェック
    public function check() {
        
        $this->load->model('login_model');
        $result = $this->login_model->check_user();
        var_dump($result);
    }
    
    //ログイン成功画面（自分の投稿ページ）
    public function entrance() {
        
        $data['name'] = $this->image_model->get_user_diary();
        $data['login'] = true;
        $data['title'] = 'トップ画面';
        $this->load->view('header', $data);
        $this->load->view('firstpage', $data);
        $this->load->view('footer', $data);
    }
    
    //Viewのテスト画面
    public function sample($id = NULL){
        if($id == null)$id = 7;
        $data['image'] = $this->image_model->get_one_diary($id);
        var_dump($data);
        $data['login'] = true;
        $data['title'] = '編集画面';
        $this->load->view('header', $data);
        $this->load->view('image_arrange', $data);
        $this->load->view('footer', $data);
    }
    //設定のページ
    public function config(){
        $data['login'] = true;
        $data['title'] = '設定画面';
        $this->load->view('header', $data);
        $this->load->view('config', $data);
        $this->load->view('footer', $data);
    }
    //友達の投稿ページ
    public function friendpage(){
        $data['name'] = $this->image_model->get_friend_diary();
        $data['login'] = true;
        $data['title'] = '友達の投稿ページ';
        $this->load->view('header', $data);
        $this->load->view('friendpage', $data);
        $this->load->view('footer', $data);
    }
    //みんなの投稿ページ
    public function everypage(){
        $data['name'] = $this->image_model->get_other_diary();
        $data['login'] = true;
        $data['title'] = 'みんなの投稿ページ';
        $this->load->view('header', $data);
        $this->load->view('everypage', $data);
        $this->load->view('footer', $data);
    }
    

}
