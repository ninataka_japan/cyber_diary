<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Image extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();

        $this->load->database();
        if ($this->db->conn_id === FALSE) {
            // データベースに接続されていません。
            $res = 'error';
          
        } else {
            // データベースに接続されています。
            $res = 'success';
          
        }
        $this->load->model("image_model");
    }
    
    
    public function test_post(){
        $url['link'] = "";
        $msg = "";
        if (isset($_FILES['icon']['error']) && is_int($_FILES['icon']['error'])) {

    try {
        

        // $_FILES['upfile']['error'] の値を確認
        switch ($_FILES['icon']['error']) {
            case UPLOAD_ERR_OK: // OK
                break;
            case UPLOAD_ERR_NO_FILE:   // ファイル未選択
                $data['error'] = 'ファイルが選択されていません';
            case UPLOAD_ERR_INI_SIZE:  // php.ini定義の最大サイズ超過
            case UPLOAD_ERR_FORM_SIZE: // フォーム定義の最大サイズ超過
                $data['error'] = 'ファイルサイズが大きすぎます';
            default:
                $data['error'] = 'その他のエラーが発生しました';
        }

        $type = @exif_imagetype($_FILES['icon']['tmp_name']);
        if (!in_array($type, [IMAGETYPE_GIF, IMAGETYPE_JPEG, IMAGETYPE_PNG], true)) {
            throw new RuntimeException('画像形式が未対応です');
        }
        $path = date("Y-m-d");
        
        $extend = explode('.',$_FILES['icon']['name']);
        if (!move_uploaded_file($_FILES['icon']['tmp_name'], "./img"."/1/tmp/".$path.'.'.$extend[1])) {
            throw new RuntimeException('ファイル保存時にエラーが発生しました');
        }
 
        $data['error'] = 'ファイルは正常にアップロードされました';
        $data['path'] = $path.'.'.$extend[1];
        $result = $this->image_model->insert_picture_diary($data['path']);
        
        $url['link'] = base_url().''; 
        } catch (RuntimeException $e) {
        $data['error'] = $e->getMessage();

        }

    }
        header("Content-type: application/json; charset=UTF-8");
        echo "test";
        /*$this->output
         ->set_content_type('application/json')
         ->set_output(json_encode($url));*/
    }
    
    public function post_file(){
        $data['title'] = 'test画面';
        $data['login'] = false;
        $this->load->view("header",$data);
        $this->load->view("sample_file");
        $this->load->view("footer",$data);
    }
    
    public function get_user_diary(){
        $result = $this->image_model->get_user_diary();
        var_dump($result);
    }
    
    public function get_friend_diary(){
        $result = $this->image_model->get_friend_diary();
        var_dump($result);
    }
}